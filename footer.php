<a href="#fim-conteudo" id="fim-conteudo" class="sr-only">Fim do conte&uacute;do</a>

<!-- Rodapé -->
<div id="site-map">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
            <?php
                wp_nav_menu( array(
                    'menu'              => 'sitemap',
                    'theme_location'    => 'footer',
                    'depth'             => 2,
                    'container'         => false,
                    'container_class'   => 'collapse navbar-collapse',
                    'container_id'      => 'menu-principal',
                    'menu_id'           => 'footer-menu',
                    'fallback_cb'       => false)
                );
            ?>
            </div>
        </div>
    </div>
</div>

<div id="campus">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <p>
                    <span>CAMPI:</span> Alvorada, Bento Gon&ccedil;alves, Canoas, Caxias do Sul, Erechim, Farroupilha, Feliz, Ibirub&aacute;, Os&oacute;rio, Porto Alegre, Restinga, Rio Grande, Rolante, Sert&atilde;o, Vacaria, Veran&oacute;polis e Viam&atilde;o.
                </p>
            </div>
        </div>
    </div>
</div>

<footer role="contentinfo">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-3">
                <a href="http://www.ifrs.edu.br/site" title="Site do IFRS" id="footer-logo"><img class="center-block img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/logo-ifrs-branco.png" alt="Logo do IFRS"/></a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6">
                <address id="footer-address">
                    <p>Rua General Os&oacute;rio, 348 | Bairro Centro</p>
                    <p>CEP: 95700-000 | Bento Gonçalves/RS</p>
                    <p>Email: <a href="mailto:pssolicitacoes@ifrs.edu.br">pssolicitacoes@ifrs.edu.br</a></p>
                    <p>Telefone: (54) 3449-3300</p>
                </address>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="row">
                    <div class="col-xs-12">
                        <?php get_search_form(); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <a href="http://www.facebook.com/IFRSOficial" class="social-link" title="Facebook"><img src="<?php echo get_template_directory_uri(); ?>/img/icone-facebook.png" /></a>
                        <a href="http://www.twitter.com/if_rs" class="social-link" title="Twitter"><img src="<?php echo get_template_directory_uri(); ?>/img/icone-twitter.png" /></a>
                        <a href="http://www.youtube.com/user/ComunicaIFRS" class="social-link" title="YouTube"><img src="<?php echo get_template_directory_uri(); ?>/img/icone-youtube.png" /></a>
                        <a href="http://instagram.com/IFRSOficial" class="social-link" title="Instagram"><img src="<?php echo get_template_directory_uri(); ?>/img/icone-instagram.png" /></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<div id="footer-bottom">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <p>
                    <!-- Wordpress -->
                    <a href="http://www.wordpress.org/" target="blank">Desenvolvido com Wordpress<span class="sr-only"> (abre uma nova p&aacute;gina)</span></a> <span class="glyphicon glyphicon-new-window"></span>
                    &mdash;
                    <!-- Código-fonte GPL -->
                    <a href="https://bitbucket.org/ricardomoro/ps-2016-1/" target="blank">C&oacute;digo-fonte deste tema sob a licen&ccedil;a GPLv3<span class="sr-only"> (abre uma nova p&aacute;gina)</span></a> <span class="glyphicon glyphicon-new-window"></span>
                    &mdash;
                    <!-- Creative Commons -->
                    <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/" target="blank"><img src="<?php echo get_template_directory_uri(); ?>/img/cc-by-nc-nd.png" alt="M&iacute;dia licenciada sob a Licen&ccedil;a Creative Commons Atribui&ccedil;&atilde;o-N&atilde;oComercial-SemDeriva&ccedil;&otilde;es 4.0 Internacional (abre uma nova p&aacute;gina)" /></a> <span class="glyphicon glyphicon-new-window"></span>
                </p>
            </div>
        </div>
    </div>
</div>

<?php wp_footer(); ?>

</body>
</html>
