<?php get_header(); ?>

<section class="container">
    <div class="row">
        <div class="col-xs-12 col-md-7">
            <?php echo get_template_part('partials/loop-avisos'); ?>
        </div>
        <div class="col-xs-12 col-md-5">
            <?php echo get_template_part('partials/banners'); ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>
