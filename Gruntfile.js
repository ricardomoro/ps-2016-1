module.exports = function(grunt) {
grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    exec: {
        deploy: {
            cmd: function(remotePath) {
                if (remotePath) {
                    return 'rsync -avzh --delete --progress ./dist/ ' + remotePath;
                }
                return;
            }
        },
    },

    imagemin: {
        dynamic: {
            files: [{
                expand: true,
                cwd: 'img/',
                src: ['*.{png,jpg,gif}'],
                dest: 'img/',
            }],
        },
    },

    less: {
        c: {
            options: {
                paths: ["less"],
            },
            files: {
                "css/ps-2016.css": "less/ps-2016.less",
            },
        },
    },

    cssmin: {
        options: {
            keepSpecialComments: 0,
        },
        target: {
            files: [{
                expand: true,
                cwd: 'css',
                src: ['*.css', '!*.min.css'],
                dest: 'css',
                ext: '.min.css',
            }],
        },
    },

    copy: {
        dist: {
            expand: true,
            cwd: '.',
            src: ['**', '!.**', '!less/**', '!node_modules/**', '!vendor/**', '!Gruntfile.js', '!package.json'],
            dest: 'dist/',
        },
    },

    clean: {
        dist: {
            src: ['dist'],
        },
    },

    watch: {
        options: {
            livereload: true,
        },
        php: {
            files: '**/*.php',
            tasks: [],
        },
        less: {
            files: 'less/*.less',
            tasks: ['less:c'],
        },
    },
});

    // Load plugins
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-exec');


    // Default task(s).
    grunt.registerTask('default', ['watch']);
    grunt.registerTask('build', [
        'clean',
        'imagemin',
        'less',
        'cssmin'
    ]);
    grunt.registerTask('dist', [
        'build',
        'copy'
    ]);
};
