<?php get_header(); ?>

<section class="container">
    <div class="row">
        <div class="col-xs-12 col-md-8">
            <?php echo get_template_part('partials/resultados'); ?>
        </div>
        <div class="col-xs-12 col-md-4">
            <?php echo get_template_part('partials/banners'); ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>
