<?php get_header(); ?>

<section class="container" id="content">
    <div class="row">
        <?php while ( have_posts() ) : the_post(); ?>
            <div class="col-xs-12 col-md-8">
                <article class="post content">
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="title"><?php the_title(); ?></h2>
                            <p>
                            <?php
                                $tipo = get_the_terms(get_the_ID(), 'tipo');
                                foreach ($tipo as $key => $value) :
                            ?>
                                    <span class="label label-info"><?php echo $value->name; ?></span>
                            <?php
                                endforeach;
                            ?>
                            </p>
                            <br/>
                            <?php the_content(); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                        <?php
                            $args = array(
                                'post_parent' => get_the_ID(),
                                'post_type' => 'edital',
                                'orderby' => 'menu_order',
                            );
                            $subpages_query = new WP_Query($args);
                        ?>
                        <?php while ( $subpages_query->have_posts() ) : $subpages_query->the_post(); ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3><?php the_title(); ?></h3>
                                </div>
                                <div class="panel-body">
                                    <?php the_content(); ?>
                                </div>
                            </div>
                        <?php endwhile; ?>
                        <?php wp_reset_postdata(); ?>
                        </div>
                    </div>
                </article>
            </div>
        <?php endwhile;?>

        <div class="col-xs-12 col-md-4">
            <?php echo get_template_part('partials/banners'); ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>
