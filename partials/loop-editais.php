<?php if (have_posts()) : ?>
    <div class="list-group">
        <?php while ( have_posts() ) : the_post(); ?>
        <a href="<?php the_permalink(); ?>" rel="bookmark" class="list-group-item">
            <h4 class="list-group-item-heading"><?php the_title(); ?></h4>
            <p class="list-group-item-text"><small>publicado em <?php the_time('d'); ?> de <?php the_time('F'); ?> de <?php the_time('Y'); ?></small></p>
        </a>
        <?php endwhile; ?>
    </div>
<?php else : ?>
    <div class="alert alert-warning" role="alert">
        <p><strong>Aguarde!</strong> Em breve os editais ser&atilde;o publicados.</p>
    </div>
<?php endif; ?>
