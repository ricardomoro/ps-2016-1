<?php
    $campi = get_terms('campus');

    $resultados = new WP_Query($args = array(
        'post_type' => 'resultado',
        'post_status' => 'publish',
    ));
?>
<?php if ($resultados->have_posts()) : ?>
    <div class="row" id="resultados">
        <div class="col-xs-12">
            <div class="title">
                <h2>Resultados</h2>
            </div>
            <div class="content">
                <p>Escolha um Campus:</p>
                <?php foreach ($campi as $key => $campus) : ?>
                        <a class="btn btn-primary" href="<?php echo get_term_link($campus); ?>"><?php echo $campus->name; ?></a>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
