<?php
    global $wp_query;
    $args = array(
        'tax_query' => array(
            array(
                'taxonomy' => 'category',
                'field'    => 'slug',
                'terms'    => 'avisos',
            ),
        ),
    );
    $args = array_merge($wp_query->query_vars, $args);
    $avisos = new WP_Query($args);
?>

<?php if ($avisos->have_posts()) : ?>
    <?php while ($avisos->have_posts()) : $avisos->the_post(); ?>
        <div class="row">
            <div class="col-xs-12">
                <article class="home-aviso">
                    <div class="row">
                        <div class="col-xs-4 col-sm-2">
                            <div class="aviso-data">
                                <span class="dia"><?php the_time('d'); ?></span>
                                <br/>
                                <?php echo str_replace('.', '', get_the_time('M')); ?>
                            </div>
                        </div>
                        <div class="col-xs-8 col-sm-10">
                            <div class="aviso-titulo">
                                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                            </div>
                            <p><?php the_excerpt(); ?></p>
                            <?php $cats = get_the_category(); ?>
                            <?php if (!empty($cats)) : ?>
                                <span class="aviso-categorias-title">Categorias:</span>
                                <ul class="aviso-categorias">
                                    <?php foreach ($cats as $key => $cat) : ?>
                                        <li><a href="<?php echo get_category_link( $cat->term_id ); ?>"><?php echo $cat->name; ?></a></li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                            <a href="<?php the_permalink(); ?>" class="btn btn-primary btn-sm pull-right">Leia mais<span class="sr-only">&nbsp;sobre &quot;<?php the_title(); ?>&quot;</span></a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    <?php endwhile; ?>
    <?php next_posts_link( '<span class="glyphicon glyphicon-arrow-left"></span> Avisos anteriores', $avisos->max_num_pages ); ?>
    <?php previous_posts_link( 'Pr&oacute;ximos Avisos <span class="glyphicon glyphicon-arrow-right"></span>' ); ?>
    <?php wp_reset_postdata(); ?>
<?php endif; ?>
