<?php
function custom_excerpt_length( $length ) {
    return 18;
}

add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

// function new_excerpt_more( $more ) {
//     return '... <a href="'. get_permalink( get_the_ID() ) . '">[leia mais<span class="sr-only"> sobre &quot;'.get_the_title().'&quot;</span>]</a>';
// }
//
// add_filter( 'excerpt_more', 'new_excerpt_more' );
