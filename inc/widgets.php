<?php
function widgets_init() {
	register_sidebar(array(
		'name' => 'Banners Laterais',
		'id' => 'banners',
		'description' => 'Banners laterais.',
		'before_widget' => ' <!--widget--><div id="%1$s" class="widget banner %2$s">',
		'after_widget'  => '<div class="clearfix"></div> </div><!--//widget-->',
		'before_title'  => '<h3 class="sr-only">',
		'after_title'   => '</h3> ',
	));
}
add_action( 'widgets_init', 'widgets_init' );
