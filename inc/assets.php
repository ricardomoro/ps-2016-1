<?php
function load_styles_2015() {
    // wp_enqueue_style( $handle, $src, $deps, $ver, $media );
    wp_enqueue_style('css-ingresso');
    wp_enqueue_style('css-prettyPhoto');

    if (WP_DEBUG) {
        wp_enqueue_style('css-ps-2016', get_stylesheet_directory_uri().'/css/ps-2016.css', array(), false, 'all');
    } else {
        wp_enqueue_style('css-ps-2016', get_stylesheet_directory_uri().'/css/ps-2016.min.css', array(), false, 'all');
    }

    if (is_post_type_archive( 'curso' ) || is_tax( 'campus' ) || is_tax( 'modalidade' )) {
        wp_enqueue_style('css-dataTables');
        wp_enqueue_style('css-dataTables-bootstrap');
    }
}

function load_scripts_2015() {
    // wp_enqueue_script( $handle, $src, $deps, $ver, $in_footer );
    wp_enqueue_script('html5shiv');
    wp_enqueue_script('html5shiv-print');
    wp_enqueue_script('respond');
    wp_enqueue_script('respond-matchmedia');
    wp_enqueue_script('jquery');
    wp_enqueue_script('bootstrap');
    wp_enqueue_script('jquery-prettyPhoto');
    wp_enqueue_script('prettyPhoto-config');
    wp_enqueue_script('js-barra-brasil');

    if (is_post_type_archive( 'curso' ) || is_tax( 'campus' ) || is_tax( 'modalidade' )) {
        wp_enqueue_script('jquery-dataTables');
        wp_enqueue_script('jquery-dataTables-bootstrap');
        wp_enqueue_script('dataTables-pt_BR');
    }
}

add_action( 'wp_enqueue_scripts', 'load_styles_2015', 2 );
add_action( 'wp_enqueue_scripts', 'load_scripts_2015', 2 );
