<?php get_header(); ?>

<section class="container" id="content">
    <div class="row">
        <div class="col-xs-12 col-md-8">
            <div class="post content">
                <h2 class="post-title">Resultados do Campus <?php single_tag_title(); ?></h2>
                <p>Selecione a modalidade abaixo.</p>
                <div id="resultados-campus">
                    <?php $terms = get_terms('modalidade'); ?>
                    <?php $resultados = array(); ?>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <?php foreach ($terms as $key => $modalidade) : ?>
                            <?php
                                global $wp_query;
                                $args = array(
                                    'post_type' => 'resultado',
                                    'orderby' => 'date',
                                    'order' => 'ASC',
                                    'modalidade' => $modalidade->slug,
                                    'posts_per_page' => 99999,
                                );
                                $args = array_merge($wp_query->query_vars, $args);
                                $resultados[$key] = new WP_Query($args);
                            ?>
                            <?php if($resultados[$key]->found_posts > 0) : ?>
                                <li role="presentation"><a href="#tab<?php echo $modalidade->term_id; ?>" aria-controls="tab<?php echo $modalidade->term_id; ?>" role="tab" data-toggle="tab"><?php echo $modalidade->name; ?></a></li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <?php foreach ($terms as $key => $modalidade) : ?>
                            <div role="tabpanel" class="tab-pane" id="tab<?php echo $modalidade->term_id; ?>">
                                <?php if ($resultados[$key]->have_posts()) : ?>
                                    <div class="list-group">
                                        <?php while ($resultados[$key]->have_posts()) : $resultados[$key]->the_post(); ?>
                                            <a href="<?php the_permalink(); ?>" rel="bookmark" class="list-group-item">
                                                <h4 class="list-group-item-heading"><?php the_title(); ?>&nbsp;<span class="label label-info"><?php echo $modalidade->name; ?></span></h4>
                                                <p class="list-group-item-text"><small><?php the_time('d/m/Y'); ?></small></p>
                                            </a>
                                        <?php endwhile; ?>
                                    </div>
                                <?php else : ?>
                                    <div class="alert alert-warning" role="alert">
                                        <p><strong>Ops!</strong>&nbsp;Ainda n&atilde;o existem resultados para essa modalidade.</p>
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-4">
            <?php echo get_template_part('partials/banners'); ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>
