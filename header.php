<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <!-- Meta Init -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index,follow">
    <meta name="author" content="Diretoria de Comunicação do IFRS">
    <meta name="keywords" content="ifrs, processo, seletivo, 2016/1, vestibular, ingresso">
    <?php echo get_template_part('partials/title'); ?>
    <link rel="alternate" type="application/rss+xml" title="<?= get_bloginfo('name'); ?> Feed" href="<?= esc_url(get_feed_link()); ?>">
    <!-- Favicon -->
    <?php echo get_template_part('partials/favicons'); ?>
    <!-- CSS, JS & etc. -->
    <?php wp_head(); ?>
</head>

<body>
    <?php echo get_template_part('partials/barrabrasil'); ?>

    <!-- Cabeçalho -->
    <div id="header-bg-line"></div>

    <header class="container-fluid">
        <h1 class="sr-only"><?php bloginfo('name'); ?></h1>
        <div class="row">
            <div class="col-xs-12 col-md-6 col-lg-7">
                <a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/topo-logo-ps-2016.png" alt="Marca do IFRS" class="center-block img-responsive"/></a>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-5 hidden-xs hidden-sm">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/topo-complemento.png" alt="Mais de 4000 vagas - Cursos T&eacute;cnicos e Superiores" class="img-responsive"/>
            </div>
        </div>
    </header>

    <!-- Menu -->
    <div class="container menu">
        <div class="row">
            <?php echo get_template_part('partials/menu'); ?>
        </div>
    </div>

    <a href="#inicio-conteudo" id="inicio-conteudo" class="sr-only">In&iacute;cio do conte&uacute;do</a>

    <div class="container">
        <?php
            if (!is_post_type_archive( 'resultado' )) {
                echo get_template_part('partials/resultados');
            }
        ?>
    </div>

    <?php breadcrumb(); ?>
