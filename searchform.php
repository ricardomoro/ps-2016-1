<form role="search" method="get" class="search-form form-inline" action="<?= esc_url(home_url('/')); ?>">
    <label class="sr-only" for="search-field">Buscar por:</label>
    <div class="input-group">
        <input type="search" value="<?= get_search_query(); ?>" name="s" id="search-field" class="search-field form-control" placeholder="Buscar em todo o site" required>
        <span class="input-group-btn">
            <button type="submit" class="search-submit btn btn-primary" title="Buscar"><span class="glyphicon glyphicon-search"></span></button>
        </span>
    </div>
</form>
