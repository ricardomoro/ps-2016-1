<?php
    global $wp_query;
    $args = array(
        'posts_per_page' => 99999,
    );
    $args = array_merge($wp_query->query_vars, $args);
    query_posts($args);
?>

<?php get_header(); ?>

<section class="container" id="content">
    <div class="row">
        <div class="col-xs-12 col-md-8">
            <div class="content">
                <h2 class="title">Editais do tipo &quot;<?php echo single_term_title(); ?>&quot;</h2>
                <?php echo get_template_part('partials/loop', 'editais'); ?>
            </div>
        </div>
        <div class="col-xs-12 col-md-4">
            <?php echo get_template_part('partials/banners'); ?>
            <?php echo get_template_part('partials/edital', 'tipos'); ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>
