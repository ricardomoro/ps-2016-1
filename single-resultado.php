<?php get_header(); ?>

<?php the_post(); ?>

<section class="container">
    <div class="row">
        <div class="col-xs-12 col-md-8">
            <article class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="post-title">
                            <?php the_title(); ?>
                            <small class="pull-right">
                            <?php
                                $modalidades = get_the_terms(get_the_ID(), 'modalidade');
                                foreach ($modalidades as $key => $modalidade) :
                            ?>
                                    <span class="label label-info"><?php echo $modalidade->name; ?></span>
                            <?php
                                endforeach;
                            ?>
                            <?php
                                $campi = get_the_terms(get_the_ID(), 'campus');
                                foreach ($campi as $key => $campus) :
                            ?>
                                    <span class="label label-primary"><?php echo $campus->name; ?></span>
                            <?php
                                endforeach;
                            ?>
                            </small>
                        </h2>

                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                    <?php if (get_post_meta( $post->ID, 'aprovados_file', true ) && get_post_meta( $post->ID, 'matricula_file', true )) : ?>
                        <div class="btn-group" role="group" aria-label="Downloads">
                            <a href="<?php echo get_post_meta( $post->ID, 'aprovados_file', true ); ?>" title="Iniciar o download da Lista de Aprovados" class="btn btn-success"><span class="glyphicon glyphicon-download-alt"></span> Lista de Aprovados</a>
                            <a href="<?php echo get_post_meta( $post->ID, 'matricula_file', true ); ?>" title="Iniciar o download das Informa&ccedil;&otilde;es para Matr&iacute;cula" class="btn btn-default"><span class="glyphicon glyphicon-download-alt"></span> Informa&ccedil;&otilde;es para Matr&iacute;cula</a>
                        </div>
                    <?php endif; ?>
                        <div class="post-content">
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="row post-meta">
                    <div class="col-xs-12">
                        <p class="post-date">Publicado em <?php the_date('d/m/Y'); ?></p>
                    </div>
                </div>
            </article>
        </div>
        <div class="col-xs-12 col-md-4">
            <div class="row">
                <div class="col-xs-12">
                    <?php echo get_template_part('partials/banners'); ?>
                    <br/>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
